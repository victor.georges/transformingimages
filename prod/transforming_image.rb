require 'mini_magick'

image= MiniMagick::Image.open(ARGV[0])
#image.colorspace 'gray'
#image.write('output.jpg')

ARGV.each do |arg|
    case
    when arg == "black_and_white"
        puts 'para preto e branco'
        image.colorspace 'gray'
    when arg.include?("resizing")
        image.resize arg.split("=")[1]
        puts 'mudando de tamanho'
    when arg.include?("watermark")
        image.combine_options do|comb|
            comb.gravity "SouthEast"
            comb.draw "image Over 0,0 0,0 #{arg.split("=")[1]}"
        end
    end
end
image.write('output.jpg')


#como código para rodar ruby img_trans.rb exp.jpg black_and_white resize="1000x1000" watermark="logo.png"